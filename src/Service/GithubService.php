<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\HealthStatus;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class GithubService
{
    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function getHealthReport(string $dinosaurName): HealthStatus
    {
        $health = HealthStatus::HEALTHY;

        $response = $this->httpClient->request('GET', 'https://api.github.com/repos/SymfonyCasts/dino-park/issues');

        $this->logger->info(
            'Request Dino Issues:',
            [
                'dino' => $dinosaurName,
                'responseStatus' => $response->getStatusCode(),
            ]
        );

        foreach ($response->toArray() as $issue) {
            if (!str_contains($issue['title'], $dinosaurName)) {
                continue;
            }
            $health = $this->getDinoStatusFromLabels($issue['labels']);
        }

        return $health;
    }

    private function getDinoStatusFromLabels(array $labels): HealthStatus
    {
        $health = HealthStatus::HEALTHY;
        foreach ($labels as $label) {
            $labelName = $label['name'];
            if (!str_starts_with($labelName, 'Status:')) {
                continue;
            }
            $status = trim(substr($labelName, strlen('Status:')));

            $health = HealthStatus::tryFrom($status);
            if (null === $health) {
                throw new RuntimeException(sprintf('%s is an unknown status label!', $status));
            }
        }

        return $health;
    }
}
