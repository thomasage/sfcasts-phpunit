<?php

namespace App\Enum;

enum HealthStatus: string
{
    case HEALTHY = 'Healthy';
    case HUNGRY = 'Hungry';
    case SICK = 'Sick';
}
